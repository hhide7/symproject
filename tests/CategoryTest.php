<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $category = new Category();

        // Test ID
        $this->assertNull($category->getId());

        // Test nom
        $nom = 'Category Name';
        $category->setNom($nom);
        $this->assertEquals($nom, $category->getNom());

        // Test description
        $description = 'Category Description';
        $category->setDescription($description);
        $this->assertEquals($description, $category->getDescription());

        // Test slug
        $slug = 'category-name';
        $category->setSlug($slug);
        $this->assertEquals($slug, $category->getSlug());
    }

    public function testIsTrue()
    {
        $category = new Category();
        $nom = 'Category Name';
        $description = 'Category Description';
        $slug = 'category-name';

        $category->setNom($nom);
        $category->setDescription($description);
        $category->setSlug($slug);

        $this->assertTrue($category->getNom() === $nom);
        $this->assertTrue($category->getDescription() === $description);
        $this->assertTrue($category->getSlug() === $slug);
    }

    public function testIsFalse()
    {
        $category = new Category();
        $nom = 'Category Name';
        $description = 'Category Description';
        $slug = 'category-name';

        $category->setNom($nom);
        $category->setDescription($description);
        $category->setSlug($slug);

        $this->assertFalse($category->getNom() === 'Wrong Name');
        $this->assertFalse($category->getDescription() === 'Wrong Description');
        $this->assertFalse($category->getSlug() === 'wrong-slug');
    }

    public function testIsEmpty()
    {
        $category = new Category();

        $this->assertEmpty($category->getNom());
        $this->assertEmpty($category->getDescription());
        $this->assertEmpty($category->getSlug());
    }
}