<?php

namespace App\Tests\Entity;

use App\Entity\Blogspot;
use App\Entity\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class BlogPostUnitTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $blogspot = new Blogspot();

        // Test ID
        $this->assertNull($blogspot->getId());

        // Test content
        $content = 'This is a blog post content.';
        $blogspot->setContent($content);
        $this->assertEquals($content, $blogspot->getContent());

        // Test slug
        $slug = 'blog-post-slug';
        $blogspot->setSlug($slug);
        $this->assertEquals($slug, $blogspot->getSlug());

        // Test createdAt
        $createdAt = new DateTimeImmutable();
        $blogspot->setCreatedAt($createdAt);
        $this->assertEquals($createdAt, $blogspot->getCreatedAt());

        // Test user
        $user = new User();
        $blogspot->setUser($user);
        $this->assertEquals($user, $blogspot->getUser());
    }

    public function testIsTrue()
    {
        $blogspot = new Blogspot();
        $content = 'This is a blog post content.';
        $slug = 'blog-post-slug';
        $createdAt = new DateTimeImmutable();
        $user = new User();

        $blogspot->setContent($content);
        $blogspot->setSlug($slug);
        $blogspot->setCreatedAt($createdAt);
        $blogspot->setUser($user);

        $this->assertTrue($blogspot->getContent() === $content);
        $this->assertTrue($blogspot->getSlug() === $slug);
        $this->assertTrue($blogspot->getCreatedAt() === $createdAt);
        $this->assertTrue($blogspot->getUser() === $user);
    }

    public function testIsFalse()
    {
        $blogspot = new Blogspot();
        $content = 'This is a blog post content.';
        $slug = 'blog-post-slug';
        $createdAt = new DateTimeImmutable();
        $user = new User();

        $blogspot->setContent($content);
        $blogspot->setSlug($slug);
        $blogspot->setCreatedAt($createdAt);
        $blogspot->setUser($user);

        $this->assertFalse($blogspot->getContent() === 'Wrong content');
        $this->assertFalse($blogspot->getSlug() === 'wrong-slug');
        $this->assertFalse($blogspot->getCreatedAt() === new DateTimeImmutable('yesterday'));
        $this->assertFalse($blogspot->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $blogspot = new Blogspot();

        $this->assertEmpty($blogspot->getContent());
        $this->assertEmpty($blogspot->getSlug());
        $this->assertEmpty($blogspot->getCreatedAt());
        $this->assertEmpty($blogspot->getUser());
    }
}