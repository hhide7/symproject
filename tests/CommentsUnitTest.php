<?php

namespace App\Tests\Entity;

use App\Entity\Comments;
use App\Entity\Paints;
use App\Entity\Blogspot;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class CommentsUnitTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $comments = new Comments();

        // Test ID
        $this->assertNull($comments->getId());

        // Test author
        $author = 'John Doe';
        $comments->setAuthor($author);
        $this->assertEquals($author, $comments->getAuthor());

        // Test email
        $email = 'johndoe@example.com';
        $comments->setEmail($email);
        $this->assertEquals($email, $comments->getEmail());

        // Test content
        $content = 'This is a comment.';
        $comments->setContent($content);
        $this->assertEquals($content, $comments->getContent());

        // Test createdAt
        $createdAt = new DateTimeImmutable();
        $comments->setCreatedAt($createdAt);
        $this->assertEquals($createdAt, $comments->getCreatedAt());

        // Test paints
        $paints = new Paints();
        $comments->setPaints($paints);
        $this->assertEquals($paints, $comments->getPaints());

        // Test blogpost
        $blogpost = new Blogspot();
        $comments->setBlogpost($blogpost);
        $this->assertEquals($blogpost, $comments->getBlogpost());
    }

    public function testIsTrue()
    {
        $comments = new Comments();
        $author = 'John Doe';
        $email = 'johndoe@example.com';
        $content = 'This is a comment.';
        $createdAt = new DateTimeImmutable();
        $paints = new Paints();
        $blogpost = new Blogspot();

        $comments->setAuthor($author);
        $comments->setEmail($email);
        $comments->setContent($content);
        $comments->setCreatedAt($createdAt);
        $comments->setPaints($paints);
        $comments->setBlogpost($blogpost);

        $this->assertTrue($comments->getAuthor() === $author);
        $this->assertTrue($comments->getEmail() === $email);
        $this->assertTrue($comments->getContent() === $content);
        $this->assertTrue($comments->getCreatedAt() === $createdAt);
        $this->assertTrue($comments->getPaints() === $paints);
        $this->assertTrue($comments->getBlogpost() === $blogpost);
    }

    public function testIsFalse()
    {
        $comments = new Comments();
        $author = 'John Doe';
        $email = 'johndoe@example.com';
        $content = 'This is a comment.';
        $createdAt = new DateTimeImmutable();
        $paints = new Paints();
        $blogpost = new Blogspot();

        $comments->setAuthor($author);
        $comments->setEmail($email);
        $comments->setContent($content);
        $comments->setCreatedAt($createdAt);
        $comments->setPaints($paints);
        $comments->setBlogpost($blogpost);

        $this->assertFalse($comments->getAuthor() === 'Jane Doe');
        $this->assertFalse($comments->getEmail() === 'janedoe@example.com');
        $this->assertFalse($comments->getContent() === 'Wrong content');
        $this->assertFalse($comments->getCreatedAt() === new DateTimeImmutable('yesterday'));
        $this->assertFalse($comments->getPaints() === new Paints());
        $this->assertFalse($comments->getBlogpost() === new Blogspot());
    }

    public function testIsEmpty()
    {
        $comments = new Comments();

        $this->assertEmpty($comments->getAuthor());
        $this->assertEmpty($comments->getEmail());
        $this->assertEmpty($comments->getContent());
        $this->assertEmpty($comments->getCreatedAt());
        $this->assertEmpty($comments->getPaints());
        $this->assertEmpty($comments->getBlogpost());
    }
}