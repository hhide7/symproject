<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Paints;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;

class PaintsUnitTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $paints = new Paints();

        // Test ID
        $this->assertNull($paints->getId());

        // Test nom
        $nom = 'Beautiful Landscape';
        $paints->setNom($nom);
        $this->assertEquals($nom, $paints->getNom());

        // Test largeur
        $largeur = '150.75';
        $paints->setLargeur($largeur);
        $this->assertEquals($largeur, $paints->getLargeur());

        // Test hauteur
        $hauteur = '200.50';
        $paints->setHauteur($hauteur);
        $this->assertEquals($hauteur, $paints->getHauteur());

        // Test inProgress
        $inProgress = true;
        $paints->setInProgress($inProgress);
        $this->assertEquals($inProgress, $paints->isInProgress());

        // Test price
        $price = '999.99';
        $paints->setPrice($price);
        $this->assertEquals($price, $paints->getPrice());

        // Test release_date
        $releaseDate = new DateTime();
        $paints->setReleaseDate($releaseDate);
        $this->assertEquals($releaseDate, $paints->getReleaseDate());

        // Test publication_date
        $publicationDate = new DateTime();
        $paints->setPublicationDate($publicationDate);
        $this->assertEquals($publicationDate, $paints->getPublicationDate());

        // Test description
        $description = 'A beautiful painting';
        $paints->setDescription($description);
        $this->assertEquals($description, $paints->getDescription());

        // Test portfolio
        $portfolio = true;
        $paints->setPortfolio($portfolio);
        $this->assertEquals($portfolio, $paints->isPortfolio());

        // Test slug
        $slug = 'beautiful-landscape';
        $paints->setSlug($slug);
        $this->assertEquals($slug, $paints->getSlug());

        // Test file
        $file = 'landscape.jpg';
        $paints->setFile($file);
        $this->assertEquals($file, $paints->getFile());

        // Test user
        $user = new User();
        $paints->setUser($user);
        $this->assertEquals($user, $paints->getUser());

        // Test category
        $category = new Category();
        $paints->addCategory($category);
        $this->assertTrue($paints->getCategory()->contains($category));
        $paints->removeCategory($category);
        $this->assertFalse($paints->getCategory()->contains($category));
    }

    public function testIsTrue()
    {
        $paints = new Paints();
        $nom = 'Beautiful Landscape';
        $largeur = '150.75';
        $hauteur = '200.50';
        $inProgress = true;
        $price = '999.99';
        $releaseDate = new DateTime();
        $publicationDate = new DateTime();
        $description = 'A beautiful painting';
        $portfolio = true;
        $slug = 'beautiful-landscape';
        $file = 'landscape.jpg';
        $user = new User();

        $paints->setNom($nom);
        $paints->setLargeur($largeur);
        $paints->setHauteur($hauteur);
        $paints->setInProgress($inProgress);
        $paints->setPrice($price);
        $paints->setReleaseDate($releaseDate);
        $paints->setPublicationDate($publicationDate);
        $paints->setDescription($description);
        $paints->setPortfolio($portfolio);
        $paints->setSlug($slug);
        $paints->setFile($file);
        $paints->setUser($user);

        $this->assertTrue($paints->getNom() === $nom);
        $this->assertTrue($paints->getLargeur() === $largeur);
        $this->assertTrue($paints->getHauteur() === $hauteur);
        $this->assertTrue($paints->isInProgress() === $inProgress);
        $this->assertTrue($paints->getPrice() === $price);
        $this->assertTrue($paints->getReleaseDate() === $releaseDate);
        $this->assertTrue($paints->getPublicationDate() === $publicationDate);
        $this->assertTrue($paints->getDescription() === $description);
        $this->assertTrue($paints->isPortfolio() === $portfolio);
        $this->assertTrue($paints->getSlug() === $slug);
        $this->assertTrue($paints->getFile() === $file);
        $this->assertTrue($paints->getUser() === $user);
    }

    public function testIsFalse()
    {
        $paints = new Paints();
        $nom = 'Beautiful Landscape';
        $largeur = '150.75';
        $hauteur = '200.50';
        $inProgress = true;
        $price = '999.99';
        $releaseDate = new DateTime();
        $publicationDate = new DateTime();
        $description = 'A beautiful painting';
        $portfolio = true;
        $slug = 'beautiful-landscape';
        $file = 'landscape.jpg';
        $user = new User();

        $paints->setNom($nom);
        $paints->setLargeur($largeur);
        $paints->setHauteur($hauteur);
        $paints->setInProgress($inProgress);
        $paints->setPrice($price);
        $paints->setReleaseDate($releaseDate);
        $paints->setPublicationDate($publicationDate);
        $paints->setDescription($description);
        $paints->setPortfolio($portfolio);
        $paints->setSlug($slug);
        $paints->setFile($file);
        $paints->setUser($user);

        $this->assertFalse($paints->getNom() === 'Wrong Name');
        $this->assertFalse($paints->getLargeur() === '123.45');
        $this->assertFalse($paints->getHauteur() === '543.21');
        $this->assertFalse($paints->isInProgress() === false);
        $this->assertFalse($paints->getPrice() === '1234.56');
        $this->assertFalse($paints->getReleaseDate() === new DateTime('tomorrow'));
        $this->assertFalse($paints->getPublicationDate() === new DateTime('tomorrow'));
        $this->assertFalse($paints->getDescription() === 'A wrong description');
        $this->assertFalse($paints->isPortfolio() === false);
        $this->assertFalse($paints->getSlug() === 'wrong-slug');
        $this->assertFalse($paints->getFile() === 'wrong.jpg');
        $this->assertFalse($paints->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $paints = new Paints();

        $this->assertEmpty($paints->getNom());
        $this->assertEmpty($paints->getLargeur());
        $this->assertEmpty($paints->getHauteur());
        $this->assertEmpty($paints->getPrice());
        $this->assertEmpty($paints->getReleaseDate());
        $this->assertEmpty($paints->getPublicationDate());
        $this->assertEmpty($paints->getDescription());
        $this->assertEmpty($paints->getSlug());
        $this->assertEmpty($paints->getFile());
        $this->assertEmpty($paints->getUser());
        $this->assertInstanceOf(ArrayCollection::class, $paints->getCategory());
        $this->assertCount(0, $paints->getCategory());
    }
}