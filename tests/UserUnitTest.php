<?php

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $user = new User();

        // Test ID
        $this->assertNull($user->getId());

        // Test email
        $email = 'test@example.com';
        $user->setEmail($email);
        $this->assertEquals($email, $user->getEmail());
        $this->assertEquals($email, $user->getUserIdentifier());

        // Test roles
        $roles = ['ROLE_ADMIN'];
        $user->setRoles($roles);
        $this->assertEquals(array_unique(array_merge($roles, ['ROLE_USER'])), $user->getRoles());

        // Test password
        $password = 'password123';
        $user->setPassword($password);
        $this->assertEquals($password, $user->getPassword());

        // Test prenom
        $prenom = 'John';
        $user->setPrenom($prenom);
        $this->assertEquals($prenom, $user->getPrenom());

        // Test nom
        $nom = 'Doe';
        $user->setNom($nom);
        $this->assertEquals($nom, $user->getNom());

        // Test telephone
        $telephone = '1234567890';
        $user->setTelephone($telephone);
        $this->assertEquals($telephone, $user->getTelephone());

        // Test about
        $about = 'About text';
        $user->setAbout($about);
        $this->assertEquals($about, $user->getAbout());

        $user->setAbout(null);
        $this->assertNull($user->getAbout());
    }

    public function testEraseCredentials()
    {
        $user = new User();
        $user->eraseCredentials();
        // This is a placeholder, as eraseCredentials doesn't do anything in the current implementation.
        // Add assertions here if you modify eraseCredentials to do something.
        $this->assertTrue(true);
    }

    public function testIsTrue()
    {
        $user = new User();
        $email = 'test@example.com';
        $password = 'password123';
        $prenom = 'John';
        $nom = 'Doe';
        $telephone = '1234567890';
        $about = 'About text';

        $user->setEmail($email);
        $user->setPassword($password);
        $user->setPrenom($prenom);
        $user->setNom($nom);
        $user->setTelephone($telephone);
        $user->setAbout($about);

        $this->assertTrue($user->getEmail() === $email);
        $this->assertTrue($user->getPassword() === $password);
        $this->assertTrue($user->getPrenom() === $prenom);
        $this->assertTrue($user->getNom() === $nom);
        $this->assertTrue($user->getTelephone() === $telephone);
        $this->assertTrue($user->getAbout() === $about);
    }

    public function testIsFalse()
    {
        $user = new User();
        $email = 'test@example.com';
        $password = 'password123';
        $prenom = 'John';
        $nom = 'Doe';
        $telephone = '1234567890';
        $about = 'About text';

        $user->setEmail($email);
        $user->setPassword($password);
        $user->setPrenom($prenom);
        $user->setNom($nom);
        $user->setTelephone($telephone);
        $user->setAbout($about);

        $this->assertFalse($user->getEmail() === 'wrong@example.com');
        $this->assertFalse($user->getPassword() === 'wrongpassword');
        $this->assertFalse($user->getPrenom() === 'Jane');
        $this->assertFalse($user->getNom() === 'Smith');
        $this->assertFalse($user->getTelephone() === '0987654321');
        $this->assertFalse($user->getAbout() === 'Wrong about text');
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getTelephone());
        $this->assertEmpty($user->getAbout());
    }
}