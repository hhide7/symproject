<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    /**
     * @var Collection<int, Paints>
     */
    #[ORM\ManyToMany(targetEntity: Paints::class, mappedBy: 'category')]
    private Collection $paints;

    public function __construct()
    {
        $this->paints = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, Paints>
     */
    public function getPaints(): Collection
    {
        return $this->paints;
    }

    public function addPaint(Paints $paint): static
    {
        if (!$this->paints->contains($paint)) {
            $this->paints->add($paint);
            $paint->addCategory($this);
        }

        return $this;
    }

    public function removePaint(Paints $paint): static
    {
        if ($this->paints->removeElement($paint)) {
            $paint->removeCategory($this);
        }

        return $this;
    }
}
