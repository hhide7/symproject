image: php:8.2-apache

before_script:
    # Update and install necessary dependencies
    - apt-get update && apt-get install -y git libzip-dev unzip wget libonig-dev libpng-dev
    # Install PHP extensions
    - docker-php-ext-install pdo_mysql zip
    # Install Composer
    - curl -sS https://getcomposer.org/installer | php
    - mv composer.phar /usr/local/bin/composer
    # Install PHP QA tools
    - composer global require "squizlabs/php_codesniffer=*"
    - composer global require "phpstan/phpstan"
    - wget https://github.com/friendsoftwig/twigcs/releases/download/v5.0.0/twigcs.phar -O /usr/local/bin/twigcs
    - chmod +x /usr/local/bin/twigcs
    # Install local-php-security-checker
    - wget https://github.com/fabpot/local-php-security-checker/releases/download/v2.1.3/local-php-security-checker_linux_amd64 -O /usr/local/bin/local-php-security-checker
    - chmod +x /usr/local/bin/local-php-security-checker
    # Add Composer global bin to PATH
    - export PATH="$HOME/.composer/vendor/bin:$PATH"
    # Install project dependencies
    - composer install

cache:
    paths:
        - vendor/
        - $HOME/.composer/cache

stages:
    - SecurityChecker
    - CodingStandards
    - UnitTests

security-checker:
    stage: SecurityChecker
    script:
        - local-php-security-checker --path=./composer.lock
    allow_failure: false

phpcs:
    stage: CodingStandards
    script:
        - phpcbf --standard=PSR12 ./src
        - phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src
    allow_failure: false

phpstan:
    stage: CodingStandards
    script:
        - phpstan analyse ./src
    allow_failure: false

twig-lint:
    stage: CodingStandards
    script:
        - /usr/local/bin/twigcs ./templates
    allow_failure: false

phpunit:
    stage: UnitTests
    script:
        - php bin/phpunit
    allow_failure: false