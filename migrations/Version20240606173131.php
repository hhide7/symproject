<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240606173131 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE paints_category (paints_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_15588F51CAE3451D (paints_id), INDEX IDX_15588F5112469DE2 (category_id), PRIMARY KEY(paints_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE paints_category ADD CONSTRAINT FK_15588F51CAE3451D FOREIGN KEY (paints_id) REFERENCES paints (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE paints_category ADD CONSTRAINT FK_15588F5112469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE paints ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE paints ADD CONSTRAINT FK_988A3048A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_988A3048A76ED395 ON paints (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE paints_category DROP FOREIGN KEY FK_15588F51CAE3451D');
        $this->addSql('ALTER TABLE paints_category DROP FOREIGN KEY FK_15588F5112469DE2');
        $this->addSql('DROP TABLE paints_category');
        $this->addSql('ALTER TABLE paints DROP FOREIGN KEY FK_988A3048A76ED395');
        $this->addSql('DROP INDEX IDX_988A3048A76ED395 ON paints');
        $this->addSql('ALTER TABLE paints DROP user_id');
    }
}
